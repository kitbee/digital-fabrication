---
title: ""
date: 2022-02-09T21:09:20+02:00
draft: false
---

# Useful Links

### FabAcademy Links
[Assignment Assessment Guide](https://fabacademy.org/2022/nueval/)

---

### Website Related
[Repo on Gitlab](https://gitlab.com/kitbee/digital-fabrication)

[Hugo Documentation](https://gohugo.io/documentation/)

[Cupper Theme Documentation](https://themes.gohugo.io/themes/cupper-hugo-theme/)

---

### Tool Links

[iTerm](https://iterm2.com/)  
Mac OS terminal replacement that I use.


[Affinity Photo, Designer, Publisher](https://affinity.serif.com/en-us/)  
This is a program I bought recently that's similar to Adobe Photoshop, Illustrator, and InDesign, however it has a one time fee rather than subscription.


[LICEcap](https://www.cockos.com/licecap/)  
For capturing GIFs of your desktop

[ImageMagick](https://imagemagick.org/)  
For manipulating images using the terminal. Allows for batch processing of images (many images at once).

---

### Inspiration Links

https://fabacademy.org/2022/labs/ciudadmexico/students/josemanuel-diaz/a3.html
