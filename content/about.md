---
title: "About Me"
date: 2022-02-02T13:30:54+02:00
draft: false
---

[@thektstarks](https://www.instagram.com/thektstarks/)

🍁🌈🪴🎮🔥🌎✨

My name is Katie and I make new media art.

![profile picture](profile.jpg)

From Canada, spent some time in the US, now in Finland.

I did an undergrad degree in Digital Futures, which included a variety of things like rapid	prototyping with laser cutters and 3D printers, interactive design principles, game theory and design, and lots of other assorted goodies. After I graduated I spent some time teaching kids maker skills, then trying my hand at design for a tech company, and eventually waded my way into the software engineering world. I spent my time there mastering imposter syndrome*, growing a bunch of avocados, and deciding I really didn't want to be there anymore, so I followed my dreams to pursue my art education once more and came to Aalto.




\* I mean I guess I did some coding but it sure as heck didn't feel like it 😅
