---
title: "04 | Electronics Production"
date: 2022-02-18T16:31:31+02:00
draft: false
---

## Electronics Milling

> **Group assignment:**  
> Characterize the design rules for your in-house PCB production process: document feeds, speeds, plunge rate, depth of cut (traces and > outline) and tooling.
> document your work (in a group or individually)
> Document your work to the group work page and reflect on your individual page what you learned
>
> **Individual assignment:**  
> Make an in-circuit programmer that includes a microcontroller by milling and stuffing the PCB, test it to verify that it works.

> **Have you answered these questions?**  
> - [ ] Linked to the group assignment page
> - [ ] Documented how you made (mill, stuff, solder) the board
> - [ ] Documented that your board is functional
> - [ ] Explained any problems and how you fixed them
> - [ ] Included a ‘hero shot’ of your board

### Resources

https://gitlab.fabcloud.org/pub/programmers/programmer-updi-d11c

http://mods.cba.mit.edu/

### Summary

- milled UPDI board

![udpi board](udpi.jpg)
![milling navigation](milling-nav.jpg)
![milling cut operation](milling-cut.jpg)
![board milled](board-milled.jpg)
![board soldering](board-soldering.jpg)
![board programming](board-programming.jpg)








### Group Project

Arthur & Katie

![group photo](group-photo.jpg)
![mods](mods.jpg)
![linetest](linetest.png)
![board milling](board-milling.jpg)
![ruler milled](ruler-milled.jpg)

We're using Mods to make a file for milling our own circuit board.

Empty window > right click > select programs > open server program > Machines: Roland: SMR-20: PCB png.

ok it pulls up a bunch of windows,  
