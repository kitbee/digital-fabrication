---
title: "Final Project"
date: 2022-01-24T16:01:15+02:00
draft: false
---

## Initial Idea Exploration

**Timer with pushbutton reset**

<img src="idea/final-idea.png" width="200px"></img>

I would like to create a more intuitive way to know when to do a task, such as watering my plants. Each plant has a different watering time. I'd like to be able to look at something and know it needs attention. So my idea is to create a small clip or sticky device that I can put on each plant or group of plants and it will flash when I need to attend to it. When I'm done attending to it, I press the button to reset the timer to a preset amount of time and it ticks back up to that time. I'm considering having a digit display of how many days have passed, or maybe just have the LEDs show the impending task by shining brighter and brighter everyday until it's blinking. I'd like to make this networked so you could have a central interface to see all the tasks that need doing, so if one of them is in a place you're not in often you can still catch when it needs doing.

This idea was inspired by playing Stardew Valley and attending to all the machines in the game when they are ready. I'm hoping to gain intuitive understanding of when things need to be done so I don't need the little devices anymore, but on a day when my memory and focus are not so attentive, I don't miss what needs attention.

### Components

- PCB board with LED, timer, and network communication
- CAD design for different styles of body, experiment with shapes and sizes
- make mold for body
- experiement casting with different materials, possibly silicon
- network series of devices
- interface for tracking devices and logging

### Experiments

I'd like to experiment with the size, aestetic, and functionality to see what works best for different scenarios. I'd like to see what purposes I can find for these timers, and see what kind of absurdities I can play with as well. There was a subreddit that had a coded red button in it and when a reddit user pressed the button, their username was associated with a title. The longer you waited to press the button close to the timer reaching zero, the more glorious your title. It spawned it's own culture, all based off a button with a timer. I invision maybe creating a series of these in different places and people have to manage pushing them all within their different timeframes, some are quicker than others. They maybe glow rainbow for 30s when pushed to give feedback.

How would I place them? Maybe they could be placed on people as well, in necklaces, bracelets, or pins.

What kinds of casing could it have? Maybe it has a mirror finish with an LED that shines through, or a wood vaneer that hides the LED. Maybe I use fabrics, fiber optics, or faux fur. I can experiment with diffusion of different plastics and resins as well.

<img src="idea/wood.jpeg" width="200px"></img>
<img src="idea/gems.jpeg" width="200px"></img>

<img src="idea/fur.jpeg" width="200px"></img>
<img src="idea/fiberoptic.jpeg" width="200px"></img>

I can experiment with the way the LED displays feedback, such as having multiple LEDs complete a circle, how it shows it's ready, how it shows it's been pressed or setup. I can experiment with colour, brightness, diffusion, amount of LEDs, and size of them.

Setting is also another avenue to explore. Yes I can have them in my house plant collection, or maybe I can mount them around the school in different rooms and make a scavenger hunt out of it. Maybe I make a large installation with very large networked buttons. Maybe I make a small crawl in cave with them decaled all over the ceiling like stars.

### Features to experiment with:

- Device's physical body: size, shape, colour, texture, material
- Interaction with the device, how it's reset or programmed
- How timer is displayed: digits, LEDs, physical mechanism, or sounds even
- Feedback when timer is reset
- Display when device is ready
- Setting for devices: in room, outside, on people, or on a small structure
- Context: Game, tasks, connection, or possibly an artistic meaning
