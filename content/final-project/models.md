---
title: "CAD Models"
date: 2022-02-09T15:59:21+02:00
draft: false
---

## CAD model
This is a model I made in Fusion360 as part of the CAD week, it's still a WIP right now but I plan to 3D print it, as well as use it to make a mold for casting.

![CAD model of Final Project](/digital-fabrication/weeks/week-04/14.jpg)
